const request = require('supertest')
const app = require('../../../../src/app')

describe('jobs', () => {
  describe('GET /jobs/unpaid', () => {
    it('Should give 401 error', () => {
      return request(app).get('/jobs/unpaid').expect(401)
    })
    it('Should give 200', () => {
      return request(app).get('/jobs/unpaid')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body).toEqual([{
            ContractId: 2,
            createdAt: body[0].createdAt,
            description: 'work',
            id: 2,
            paid: null,
            paymentDate: null,
            price: 201,
            updatedAt: body[0].updatedAt
          }])
        })
    })
  })

  describe('POST /jobs/:job_id/pay', () => {
    it('Should give 401 error', () => {
      return request(app).post('/jobs/1/pay').expect(401)
    })
    it('Should give 403', () => {
      return request(app).post('/jobs/1/pay')
        .set('profile_id', 1)
        .expect(403)
    })
    it('Should give 200', () => {
      return request(app).post('/jobs/2/pay')
        .set('profile_id', 1)
        .expect(200)
    })
    it('Should give 403', () => {
      return request(app).post('/jobs/2/pay')
        .set('profile_id', 1)
        .expect(403)
    })
  })
})

const request = require('supertest')
const app = require('../../../../src/app')

describe('admin', () => {
  describe('GET /admin/best-profession', () => {
    it('Should give 401 error', () => {
      return request(app).get('/admin/best-profession').expect(401)
    })
    it('Should give 422 error', () => {
      return request(app).get('/admin/best-profession')
        .set('profile_id', 1)
        .expect(422)
    })
    it('Should give 422 error', () => {
      return request(app).get('/admin/best-profession?start=2016-01-26&end=2018-02-281')
        .set('profile_id', 1)
        .expect(422)
    })
    it('Should give 200 with 0 results', () => {
      return request(app).get('/admin/best-profession?start=2016-01-26&end=2018-02-28')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body.length).toBe(0)
        })
    })
    it('Should give 200 with first result profession Programmer', () => {
      return request(app).get('/admin/best-profession?start=2016-01-26&end=2021-02-28')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body[0].profession).toBe('Programmer')
        })
    })
  })

  describe('GET /admin/best-clients', () => {
    it('Should give 401 error', () => {
      return request(app).get('/admin/best-clients').expect(401)
    })
    it('Should give 422 error', () => {
      return request(app).get('/admin/best-clients')
        .set('profile_id', 1)
        .expect(422)
    })
    it('Should give 422 error', () => {
      return request(app).get('/admin/best-clients?start=2016-01-26&end=2018-02-281')
        .set('profile_id', 1)
        .expect(422)
    })
    it('Should give 200 with 0 results', () => {
      return request(app).get('/admin/best-clients?start=2016-01-26&end=2018-02-28')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body.length).toBe(0)
        })
    })
    it('Should give 200 with first result client Ash Kethcum & paid of 2020', () => {
      return request(app).get('/admin/best-clients?start=2016-01-26&end=2021-02-28')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body[0].fullName).toBe('Ash Kethcum')
          expect(body[0].paid).toBe(2020)
          expect(body[0].id).toBe(4)
        })
    })
  })
})

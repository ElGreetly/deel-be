const request = require('supertest')
const app = require('../../../../src/app')

describe('contracts', () => {
  describe('GET /contracts/:id', () => {
    it('Should give 401 error', () => {
      return request(app).get('/contracts/1').expect(401)
    })
    it('Should give 422 error', () => {
      return request(app).get('/contracts/mmm')
        .set('profile_id', 1)
        .expect(422)
    })
    it('Should give 404 error', () => {
      return request(app).get('/contracts/123')
        .set('profile_id', 1)
        .expect(404)
    })
    it('Should give 200', () => {
      return request(app).get('/contracts/1')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body).toEqual({
            id: 1,
            ClientId: 1,
            ContractorId: 5,
            createdAt: body.createdAt,
            status: 'terminated',
            terms: 'bla bla bla',
            updatedAt: body.updatedAt
          })
        })
    })
  })

  describe('GET /contracts', () => {
    it('Should give 401 error', () => {
      return request(app).get('/contracts').expect(401)
    })
    it('Should give 200 with 1 results', () => {
      return request(app).get('/contracts')
        .set('profile_id', 1)
        .expect(200).then(({ body }) => {
          expect(body.length).toBe(1)
        })
    })
  })
})

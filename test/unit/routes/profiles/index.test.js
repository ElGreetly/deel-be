const request = require('supertest')
const app = require('../../../../src/app')

describe('profiles', () => {
  describe('POST /balances/deposit/:amount', () => {
    it('Should give 401 error', () => {
      return request(app).post('/balances/deposit/55').expect(401)
    })
    it('Should give 403', () => {
      return request(app).post('/balances/deposit/9999')
        .set('profile_id', 1)
        .expect(403)
    })
    it('Should give 200', () => {
      return request(app).post('/balances/deposit/10')
        .set('profile_id', 2)
        .expect(200).then(({ body }) => {
          console.log(body)
        })
    })
    it('Should give 403', () => {
      return request(app).post('/balances/deposit/300')
        .set('profile_id', 1)
        .expect(403)
    })
  })
})

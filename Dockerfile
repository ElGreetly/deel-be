FROM node:lts-alpine

RUN mkdir /app
WORKDIR /app


COPY package*.json ./

RUN npm i
# If you are building your code for production
# RUN npm ci --only=production

COPY . .

CMD ["sh", "-c", "node ./scripts/seedDb.js && node ./src/server.js"]
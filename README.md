# DEEL BACKEND TASK

💫 Welcome! 🎉

[Deel BE](https://greetly.dev) exercise.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

For local run

```
node version 12
npm
```

## Built With

- [Expressjs](https://expressjs.com/) - Router
- [Sequelize](https://sequelize.org/) - ORM

### Code lint

The code is linted using eslint according to the `standard` standard

## How to run locally

- Clone the repo
- Then `cd ./repoName`
- Run `npm i`
- Run `npm run seed`
- Run `npm start`

## Deployment

For Heroku deployment follow up with instruction here [Node.js Heroku app deployment](https://devcenter.heroku.com/articles/getting-started-with-nodejs)

- The Heroku `Procfile` is already committed within the repo

For docker

- Run `docker build -t deel-be .`
- Run `docker run -p 3001:3001 -d deel-be`

For docker compose

- Run `docker-compose up --build -d`

### Note

Docker above commands will deploy on port 3001 make sure to change the ports when deploying on server

## See Docs

[Deel Documentation](https://greetly.dev/docs)

## Authors

- **Ahmed ElGreetly** - _Initial work_ - [Greetly](https://github.com/ElGreetly)

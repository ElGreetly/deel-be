const express = require('express')

express.response.sendError = function (msg, code, data) {
  this.status(code).json({
    errors: [
      {
        msg,
        data
      }
    ]
  })
}

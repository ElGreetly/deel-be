const Validator = require('fastest-validator')

const validator = new Validator()

const validate = (schema) => (req, res, next) => {
  const result = validator.validate({ ...req.params, ...req.query }, schema)
  if (result === true) return next()
  res.status(422).json({ errors: result })
}

module.exports = validate

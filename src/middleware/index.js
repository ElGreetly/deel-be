module.exports = {
  getProfile: require('./getProfile'),
  validate: require('./validate')
}

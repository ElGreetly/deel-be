const { validate } = require('../../middleware')

const contractByIdValidationSchema = {
  id: { type: 'number', convert: true }
}

module.exports = {
  contractByIdValidator: validate(contractByIdValidationSchema)
}

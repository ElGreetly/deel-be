const router = require('express').Router()
const { Op } = require('sequelize')
const { contractByIdValidator } = require('./validation')

/**
 * Get contract by ID
 *
 * @returns {Contract}
 */
router.get('/:id', contractByIdValidator, async (req, res) => {
  const { Contract } = req.app.get('models')
  const { id } = req.params
  const { id: profileId } = req.profile
  const contract = await Contract.findOne({
    where: {
      id,
      [Op.or]: {
        ContractorId: profileId,
        ClientId: profileId
      }
    }
  })
  if (!contract) return res.sendError('Not Found!', 404)
  res.json(contract)
})

/**
 * Get list of logged in user active contracts
 *
 * @returns {Array<Contract>}
 */
router.get('/', async (req, res) => {
  const { Contract } = req.app.get('models')
  const { id: profileId } = req.profile
  const contracts = await Contract.findAll({
    where: {
      [Op.or]: {
        ContractorId: profileId,
        ClientId: profileId
      },
      status: {
        [Op.eq]: 'in_progress'
      }
    }
  })
  res.json(contracts)
})

module.exports = router

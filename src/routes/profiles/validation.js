const { validate } = require('../../middleware')

const depositValidationSchema = {
  amount: { type: 'number', min: 1, convert: true }
}

module.exports = {
  depositValidator: validate(depositValidationSchema)
}

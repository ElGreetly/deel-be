const router = require('express').Router()
const { QueryTypes } = require('sequelize')
const { sequelize } = require('../../model')
const { depositValidator } = require('./validation')

/**
 * Deposit credits to client account
 *
 * @returns {Object}
 */
router.post('/balances/deposit/:amount', depositValidator, async (req, res) => {
  const { profile } = req
  const { amount: depositAmount } = req.params

  // If the user not a client return
  if (profile.type !== 'client') {
    return res.sendError('You can\'t deposit any credits', 403)
  }

  // Initialize unpaid jobs total
  let unpaidJobsTotal = 0

  // Get unpaid jobs total for this client
  const queryResponse = await sequelize.query(`SELECT SUM(price)total FROM Profiles AS p
    JOIN Contracts AS c
    JOIN Jobs AS j WHERE p.id = (:profileId) AND c.clientid = p.id
    AND c.status = 'in_progress' AND j.ContractId = c.id AND j.paid is not 1;`, {
    replacements: {
      profileId: profile.id
    },
    type: QueryTypes.SELECT
  })

  if (queryResponse && queryResponse[0]) unpaidJobsTotal = queryResponse[0].total
  const totalAllowedDeposit = unpaidJobsTotal + 0.25 * unpaidJobsTotal - profile.balance
  if (totalAllowedDeposit < depositAmount) {
    return res.sendError('You can\'t deposit more than 25% of you jobs need to pay total', 403)
  }

  const transaction = await sequelize.transaction()
  try {
    await profile.increment('balance', { by: depositAmount }, { transaction })
    await transaction.commit()
  } catch (err) {
    await transaction.rollback()
    return res.sendError('Something went wrong!', 500)
  }

  res.json({
    message: 'Deposit succeed'
  })
})

module.exports = router

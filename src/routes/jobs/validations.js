const { validate } = require('../../middleware')

const payJobValidationSchema = {
  job_id: { type: 'number', convert: true }
}

module.exports = {
  payJobValidator: validate(payJobValidationSchema)
}

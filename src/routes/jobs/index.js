const router = require('express').Router()
const { Op } = require('sequelize')
const { sequelize } = require('../../model')
const { payJobValidator } = require('./validations')

/**
 * Get list of unpaid job for active contracts
 * for the logged user
 *
 * @returns {Array<Job>}
 */
router.get('/unpaid', async (req, res) => {
  const { Profile, Contract, Job } = req.app.get('models')
  const { id, type } = req.profile
  const profile = await Profile.findOne({
    where: {
      id
    },
    include: {
      model: Contract,
      as: 'C' + type.slice(1),
      where: {
        status: {
          [Op.eq]: 'in_progress'
        }
      },
      include: {
        model: Job,
        where: {
          paid: {
            [Op.not]: true
          }
        }
      }
    }
  })

  // Get the jobs from the profile
  const jobs = profile.Client.map(contract => contract.Jobs).flat()
  res.json(jobs)
})

/**
 * Pay unpaid job
 *
 * @returns {Object}
 */
router.post('/:job_id/pay', payJobValidator, async (req, res) => {
  const { profile } = req

  // Check user type
  if (req.profile.type === 'contractor') {
    return res.sendError('Contractors can\'t pay for jobs', 403)
  }

  const { Profile, Contract, Job } = req.app.get('models')

  // Get required job for payment
  const job = await Job.findOne({
    where: {
      id: req.params.job_id,
      paid: {
        [Op.not]: true
      },
      price: {
        // This could be validated within the service level to give info
        // that the balance is enough
        [Op.lte]: profile.balance
      }
    },
    include: {
      model: Contract,
      where: {
        ClientId: profile.id,
        status: {
          [Op.eq]: 'in_progress'
        }
      }
    }
  })

  // If job not found or doesn't belong to the user
  if (!job) {
    return res.sendError('Could not process the payment', 403)
  }

  // Get the contractor profile
  const contractorProfile = await Profile.findOne({ where: { id: job.Contract.ContractorId } })

  try {
    // Start the payment managed transaction
    await sequelize.transaction(async (t) => {
      // Decrease the balance from the client
      await profile.decrement('balance', { by: job.price }, { t })
      // Add the balance to the contractor
      await contractorProfile.increment('balance', { by: job.price }, { t })

      // Update the job as paid
      job.paid = true
      job.paymentDate = new Date()
      await job.save({ t })
    })
  } catch (err) {
    return res.sendError('Something went wrong', 500)
  }
  res.json({
    message: 'Job paid successfully'
  })
})

module.exports = router

const router = require('express').Router()

router.use('/contracts', require('./contracts'))
router.use('/jobs', require('./jobs'))
router.use(require('./profiles'))
router.use('/admin', require('./admin'))

module.exports = router

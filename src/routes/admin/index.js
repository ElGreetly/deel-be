const router = require('express').Router()
const { QueryTypes } = require('sequelize')
const { sequelize } = require('../../model')
const { bestProfessionValidator, bestClientValidator } = require('./validation')

/**
 * Get list of the most paid profession during a given period
 *
 * @returns {Array<Object>}
 */
router.get('/best-profession', bestProfessionValidator, async (req, res) => {
  const { start, end } = req.query
  const response = await sequelize.query(`
    SELECT DISTINCT sum(price)total, profession from Jobs AS j
    JOIN Contracts AS c
    JOIN Profiles AS p WHERE p.id = c.contractorid AND j.contractid = c.id AND j.paid = true
    AND j.paymentDate >= (:start) AND j.paymentDate <= (:end)
    GROUP BY p.profession
    ORDER BY total DESC
    `, {
    replacements: {
      start,
      end
    },
    type: QueryTypes.SELECT
  })
  res.json(response)
})

/**
 * Get list of the best clients during a given period
 *
 * @returns {Array<Object>}
 */
router.get('/best-clients', bestClientValidator, async (req, res) => {
  const { start, end, limit } = req.query
  const response = await sequelize.query(`
      SELECT p.id, SUM(price)paid, (p.firstname || ' ' || p.lastname)fullName from Profiles AS p
      JOIN Contracts AS c
      JOIN Jobs AS j WHERE c.clientid = p.id AND j.paid = true AND j.ContractId = c.id
      AND j.paymentDate >= (:start) AND j.paymentDate <= (:end)
      GROUP by fullName
      ORDER by paid DESC
      LIMIT (:limit)
      `, {
    replacements: {
      start,
      end,
      limit: +limit || 2
    },
    type: QueryTypes.SELECT
  })
  res.json(response)
})

module.exports = router

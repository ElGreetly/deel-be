const { validate } = require('../../middleware')

const bestProfessionValidationSchema = {
  start: { type: 'string', pattern: /^(20[1-9][0-9])-((0[1-9])|(1(0|1|2)))-(((0[1-9])|(1|2)[0-9])|3(0|1))$/ },
  end: { type: 'string', pattern: /^(20[1-9][0-9])-((0[1-9])|(1(0|1|2)))-(((0[1-9])|(1|2)[0-9])|3(0|1))$/ }
}

const bestClientValidationSchema = {
  start: { type: 'string', pattern: /^(20[1-9][0-9])-((0[1-9])|(1(0|1|2)))-(((0[1-9])|(1|2)[0-9])|3(0|1))$/ },
  end: { type: 'string', pattern: /^(20[1-9][0-9])-((0[1-9])|(1(0|1|2)))-(((0[1-9])|(1|2)[0-9])|3(0|1))$/ },
  limit: { type: 'number', convert: true, min: 1, optional: true }
}

module.exports = {
  bestProfessionValidator: validate(bestProfessionValidationSchema),
  bestClientValidator: validate(bestClientValidationSchema)
}

const express = require('express')
const swaggerUi = require('swagger-ui-express')
const { sequelize } = require('./model')
const { getProfile } = require('./middleware')
const app = express()

// Add sendError method to response prototype
require('./middleware/sendError')

// Require openapi file
const swaggerDocument = require('../openapi.json')

// Require app routes
const router = require('./routes')

// Body parser is available builtin in expressjs since v4.16.0
// https://expressjs.com/en/api.html#express.json
app.use(express.json())
app.set('sequelize', sequelize)
app.set('models', sequelize.models)

// Add routes middleware
app.use(['/contracts', '/jobs', '/balances', '/admin'], getProfile)

// Add the routes
app.use(router)

// Add openapi UI middleware
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use('/', (req, res) => res.redirect('/docs'))

module.exports = app
